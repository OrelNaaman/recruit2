<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Role;
use App\Department;
use App\Interview;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Status::all();    
        $interviews = Interview::all();       
        return view('interviews.index', compact('candidates','users', 'statuses','interviews',));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        // $interview->description = $request->description; 
        // $candidate->date = $request->date;
        $int = $interview->create($request->all());
        $int->save();
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $interview = Interview::findOrFail($id);
        $interview->update($request->all());
        return redirect('interviews');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = Interview::findOrFail($id);
        $interview->delete(); 
        return redirect('interviews'); 
    }
    public function myInterviews()
    {        

        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
    
        return view('interviews.index', compact('interviews', 'candidates', 'users'));
    }

    public function userInterviews()
    {        
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
    
        return view('interviews.userInterviews', compact('interviews', 'candidates', 'users'));
    }

    public function createInterview()
    {        

        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
    
        return view('interviews.create', compact('interviews', 'candidates', 'users'));
    }

    public function changecid($iid, $cid = null){
        Gate::authorize('assign-user');
        $interview = Interview::findOrFail($iid);
        $interview->cid = $cid;
        $interview->save(); 
        return back();
        //return redirect('candidates');
    }

    public function create_changecid($cid = null, $description = null, $date = null){
        Gate::authorize('assign-user');
        $interview = Interview::findOrFail($iid);
        $interview->cid = $cid;
        $interview->save(); 
        return back();
        //return redirect('candidates');
    }

}
