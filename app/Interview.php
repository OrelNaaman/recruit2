<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['description','date','cid','uid'];

    
    public function candidates()
    {
        return $this->belongsTo('App\Candidate', 'cid');
    }  

    public function users()
    {
        return $this->belongsTo('App\User', 'uid');
    }  
}
//
