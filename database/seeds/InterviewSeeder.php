<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
            'description' => 'very good resoume, move to second interview',
            'date' => Carbon::create('2020', '06', '06','07','50','00'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
                'description' => 'not fit proffesionality , thanks you and good luck',
                'date' => Carbon::create('2020', '07', '07','08','10','00'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],  
    ]); 
    }
}
