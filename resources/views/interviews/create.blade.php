@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>

        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 

        <div class="form-group">
            <label for = "name">Interview description</label>
            <input type = "text" class="form-control" name = "description">
        </div>

        <div class="form-group">
            <label for = "name">Interview date</label>
            <input type = "text" class="form-control" name = "date">
        </div>

        <div class="form-group">
            <label for = "name">Interview User</label>
            <div class="dropdown">
                <select class="form-control" name="cid">
                    <option value="{{ null }}">Assign User</option>
                    @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
                </select>
            
            </div>    
        </div>

        <div>
            <input type = "submit" name = "submit" value = "Create Interview">
        </div>                       
        
        </form>    
@endsection
