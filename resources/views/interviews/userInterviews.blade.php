@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

@if(Session::has('notallowed'))
    <div class = 'alert alert-danger'>
        {{Session::get('notallowed')}}
    </div>
@endif

<h1>List of Interviews</h1>

<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Description</th>
        <th>Candidate</th>
        <th>Date</th>
        <th>Created</th>
        <th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
    @if($interview->uid == $user->id)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->description}}</td>
            <td>
                {{$interview->candidates->name}}  
            </td>
            <td>{{$interview->date}}</td>             
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>                      
        </tr>
    @endif
    @endforeach
</table>
@endsection

