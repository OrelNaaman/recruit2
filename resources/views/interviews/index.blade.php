@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

@if(Session::has('notallowed'))
    <div class = 'alert alert-danger'>
        {{Session::get('notallowed')}}
    </div>
@endif

<div><a href =  "{{url('/interviews/create')}}"> Add new Interview</a></div>

<h1>List of Interviews</h1>

<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Description</th>
        <th>Candidate</th>
        <th>User</th>
        <th>Date</th>
        <th>Created</th>
        <th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->description}}</td>
            <td>

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       
                        @if(isset($interview->cid))
                          {{$interview->candidates->name}}  
                        @else
                          Assign Candidate
                        @endif

                    </button>
                    
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                      <a class="dropdown-item" href="{{route('interview.changecid',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a>
                    @endforeach
                    </div>
                
                </div>                
            </td>
            <td>{{$interview->date}}</td>             
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
                                                            
        </tr>
    @endforeach
</table>
@endsection

